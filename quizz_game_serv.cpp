#include <iostream>
#include <sqlite3.h>
#include <string.h>
#include <vector>
#include "dao.h"
#include <pthread.h>
#include <errno.h>
#include <netinet/in.h>
#include "th_data.h"
#include <unistd.h>

using namespace std;

#define PORT 2908

vector<Question> Dao::questions;
int playersConnected = 0;
int playersReady = 0;
int playersRegistered = 0;
vector<ThData*> th; //thread pool i think
bool playingFlag = false;

static void* treat(void* ); //will be executed by every thread that serves a client
string buildScores();

int main(int argc, char* argv[]){
    struct sockaddr_in server; //used by the server
    struct sockaddr_in from;
    int answer; //client's answer
    int sd; //socket descriptor
    int pid;

    int i = 0; //used for thread id's

    //loading 5 random questions
    Dao dao("qa.db");
    dao.loadQuestions();
    
    for(auto it = Dao::questions.begin(); it != Dao::questions.end(); it++){
        cout << "[MAIN] " << it->id << ' ' << it->text << ' ' << it->option_1 << ' ' << it->option_2 << ' ' <<
        it->option_3 << ' ' << it->answer << ' ' <<  endl;
    }

    if((sd = socket (AF_INET, SOCK_STREAM, 0)) == -1) {
        cout << "[SERV] Error: make socket" << endl;
    }

    //whatever this does
    int on = 1;
    setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

    //clearing data structures
    bzero (&server, sizeof(server));
    bzero (&from, sizeof(from));

    //filling server struct
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl (INADDR_ANY);
    server.sin_port = htons(PORT);

    //bind socket
     if (bind (sd, (struct sockaddr *) &server, sizeof (struct sockaddr)) == -1)
    {
      perror ("[server]Eroare la bind().\n");
      return -1;
    }

    //listening at PORT
    if(listen(sd, 2) == -1){
        cout << "[SERV] Error: listen" << endl;
        return -1;
    }

    //serving clients
    while(1){
        int client;
        ThData* thData = new ThData; //will pe passed to treat()
        socklen_t length = sizeof(from); //???

        cout << endl << "[SERV] Waiting at port... " << PORT << endl;

        fflush(stdout); //???

        if((client = accept (sd, (struct sockaddr *) &from, &length)) == -1) {
            cout << "[SERV] Error: accept" << endl;
        }

        //got connection

        int threadId;
        int newCli; //descriptor returned by accept();

        thData->cl = client;
        thData->idThread = i;
        thData->ingame = false;

        playersConnected++;
        th.push_back(thData);
        pthread_create(&(th.at(i)->idThread), nullptr, &treat, thData);
        i++;
    }

    return 0;
}

//why is this void since it will always receive ThData object?
static void* treat(void* arg){
    ThData* thData;
    thData = (ThData*) arg;
    thData->score = 0;

    //wait until current session is over
    if (playingFlag == true) cout << "Please wait for current session to end" << endl;
    while (playingFlag == true);

    cout << "Waiting for min 2 players..." << endl;
    while(playersConnected < 2);
    
    //ready to begin:
    bool enoughConnected = true;
    write(thData->cl, &enoughConnected, 1);

    char ready;
    read(thData->cl, &ready, 1);
    playersReady++;


    cout << playersReady << " / " << playersConnected << " ready " << endl;
    bool allReady;
    write(thData->cl, &allReady, 1);

    //get username
    try{
        if(read(thData->cl, thData->name, 30) < 0){
            cout << "Error read() id: " << thData->idThread << endl;
            throw thData->idThread;
        }
    } catch(pthread_t id){
        cout << "Caught read exception id: " << id;
    }
    cout << "Registed user: " << thData->name << endl;
    
    
    playersRegistered++;
    
    while(playersRegistered != playersConnected);

    //playersReady = 0;
    playingFlag = true;
    thData->ingame = true;

    for(int questionNumber = 0; questionNumber < 5; questionNumber++){
        int answer;

        //build and send question to client
        std::string question = Dao::questions[questionNumber].text + "\n1. " + Dao::questions[questionNumber].option_1 + " 2. " +
        Dao::questions[questionNumber].option_2 + " 3. " + Dao::questions[questionNumber].option_3;
        
        if(write(thData->cl, question.c_str(), 1024) < 0) {
            cout << "Error write() id: " << thData->idThread;
        }
        

        cout << "\nWaiting for <<ThID: " << thData->idThread << ">>...\n";

        //get answer from client
        if(read(thData->cl, &answer, sizeof(int)) < 0) {
            cout << "Error read() id: " << thData->idThread;
        }

        cout << "[ThID " << thData->idThread << "] Answer from client is: " << answer << endl;

        fflush(stdout);
        pthread_detach(pthread_self());

        //server confirmation
        if(write(thData->cl, "[SERV] Got your answer, we are evaluating!", 1024) < 0){
            cout << "Error write() id: " << thData->idThread;
        }

        //check and send if correct
        if(answer == Dao::questions[questionNumber].answer) {
            thData->score++;
            write(thData->cl, "[SERV] CORRECT\n", 1024);
        }
        else 
            write(thData->cl, "[SERV] WRONG\n", 1024);

        playersReady++;
        
        //check all ready
        while(playersReady % playersRegistered != 0);

        //update score

        //send scores to client
        write(thData->cl, buildScores().c_str(), 1024);
    }

    thData->ingame = false;
    playersRegistered--;
    playersReady = 0;
    playersConnected--;
    playingFlag = false;
    //clear non-playing still connected users
    // for(auto i = th.begin(); i != th.end(); i++){
    //     if((*i)->ingame == false) th.erase(i);
    // }

    //closing this client's connection
    close((intptr_t) arg);

    return nullptr;
}

string buildScores(){
    string scores;
    for(auto i = th.begin(); i != th.end(); i++){
        scores = scores + (*i)->name + ' ' + to_string((*i)->score) + '\n'; 
    }

    return scores;
}