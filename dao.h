#pragma once
#include<sqlite3.h>
#include"question.h"
#include<vector>
#include<iostream>

using namespace std;

#define QUERY "select * from qa order by random() limit 5"

class Dao
{
    sqlite3* db;

    static int callback(void *data, int argc, char** argv, char** colName){
    Question q;

    q.id = stoi(argv[0]);
    q.text = argv[1];
    q.option_1 = argv[2];
    q.option_2 = argv[3];
    q.option_3 = argv[4];
    q.answer = stoi(argv[5]);

    cout << "[CALLBACK] Randomly selected question...\n" << ' ' << q.id << ' ' << q.text << ' ' << q.option_1 << ' ' << q.option_2 << ' ' << q.option_3 << ' ' << q.answer << endl;
    cout << endl;
    
    Dao::questions.push_back(q);

    return 0;   
    }

public:
    static vector<Question> questions;

    Dao(const char* db_path){
        if(sqlite3_open("qa.db", &db) < 0)
            cout << "Error opening database! " << endl;
    }

    void loadQuestions(){
        if(
            sqlite3_exec(db, QUERY, callback, nullptr, nullptr) < 0
        )       cout << "Error executing query: " << QUERY << endl;
    }

};