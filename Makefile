all:
	sqlite3 qa.db ".read db_script.sql"
	g++ quizz_game_serv.cpp -std=c++11 -lsqlite3 -lpthread -o quizz_game_serv
	g++ quizz_game_cli.cpp -std=c++11 -o quizz_game_cli
clean:
	rm -f quizz_game_serv
	rm -f quizz_game_cli
	rm -f qa.db