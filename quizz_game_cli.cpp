#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/time.h>

using namespace std;

//server connection port
int port;

int main(int argc, char* argv[])
{
    int sd; //socket descriptor
    int answer = 0; //answer given by client to be interpreted by server
    struct sockaddr_in server; //used for server connection

    //check for correct command line syntax
    if (argc != 3){
        cout << "Syntax Error!" << endl << "Syntax should be: "
            << argv[0] << " <server_address> <port> " << endl;
        
        return -1;
    }

    port = atoi(argv[2]);

    //make socket
    if((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        cout << "[CLI] Error: make socket " << endl;
    }

    //fill server connection structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = inet_addr(argv[1]);
    server.sin_port = htons(port);

    if (connect (sd, (struct sockaddr *) &server,sizeof (struct sockaddr)) == -1)
    {
      perror ("[CLI] Error: connect\n");
      return errno;
    }

    cout << "Please wait for min 2 players to connect..." << endl;
    bool connected;
    read(sd, &connected, 1); //block until 2 players connected;

    //wait for more players or start
    char ready;
    cout << "2 players are connected." << endl;
    cout << "Ready? Press enter... ";
    getchar();
    write(sd, &ready, 1);

    cout << "Please wait for everyone to be ready..." << endl;
    bool allReady;
    read(sd, &allReady, 1);

    fd_set fdset_name;
    struct timeval interval;
    int user_wrote = 0;
        
    interval.tv_sec = 10;
    interval.tv_usec = 0;
    FD_ZERO(&fdset_name); //clears fd set
    FD_SET(0, &fdset_name); //adds 0 to the fd set (stdin)

    char name[30];
    cout << "Enter name: ";
    fflush(stdout);

    user_wrote = select(1, &fdset_name, nullptr, nullptr, &interval);
    if(user_wrote == -1) {
        cout << "Fatal error :(" << endl;
    }
    else if (user_wrote == 0) {
        strcpy(name, "anon");
    }
    else {
        cin >> name;
    }

    //send username
    write(sd, name, 30);

    cout << "Please wait for players to register..." << endl;

    for(int i = 0; i < 5; i++){
        //for non-blocking cin (i.e. timer)
        fd_set fdset;
        struct timeval timeout;
        user_wrote = 0;
        
        timeout.tv_sec = 10;
        timeout.tv_usec = 0;
        FD_ZERO(&fdset); //clears fd set
        FD_SET(0, &fdset); //adds 0 to the fd set (stdin)

        //get question from server
        char question[1024];
        read(sd, &question, sizeof(question));
        cout << question << endl;

        cout << "\nAnswer: ";
        fflush(stdout);

        //get input or timeout
        user_wrote = select(1, &fdset, nullptr, nullptr, &timeout);

        if(user_wrote == -1){
            cout << "Fatal error :(" << endl;
        }
        else if(user_wrote == 0){
            cout << "Timed out! :(" << endl;
            answer = -1;
        }
        else {
            cin >> answer;
        }

        //sending answer
        write(sd, &answer, sizeof(int));

        //waiting for server confirmation;
        char response[1024];
        read(sd, &response, 1024);
        cout << response << endl;

        bzero(response, sizeof(response));

        //see if answer is correct
        read(sd, &response, sizeof(response));
        cout << response << endl;
        if (i != 4) cout << "please wait for other players..." << endl;

        //get scores from server
        char scores[1024];
        read(sd, &scores, sizeof(scores));
        cout << "\nScores: " << endl;
        cout <<  scores << endl;
    }

    close(sd);

    return 0;
}