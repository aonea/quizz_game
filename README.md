## quizz_game

# Dependencies:
**sqlite3:**
`sudo apt-get install sqlite3 libsqlite3-dev` <br>
**pthread:**
`sudo apt-get install libpthread-stubs0-dev`

# Compilation and execution
Compile using the command: 
`make all` <br>
Run server using the command:
`./quizz_game_serv`<br>
Run the client using the command:
`./quizz_game_cli 127.0.0.1 2908`

# Description
A minimum of two players is required to play the game. The game can start if both clients
choose so (i.e. both are ready), or they can choose to wait for more players to connect. The game begins when everyone is ready.
Players will be prompted with 5 question and will have 10 seconds to answer each by choosing
pressing the number corresponding to the answer they see fit and pressing enter. If when you
connect a game is already in progress, you'll have to wait for said game to be finished.

# References
1. https://profs.info.uaic.ro/~computernetworks/files/NetEx/S12/ServerConcThread/servTcpConcTh2.c
2. https://profs.info.uaic.ro/~computernetworks/files/NetEx/S12/ServerConcThread/cliTcpNr.c
3. https://www.sqlite.org/cintro.html
4. https://www.tutorialspoint.com/sqlite/sqlite_c_cpp.htm
5. https://www.unix.com/programming/16678-how-get-timed-input-using-cin.html
6. https://stackoverflow.com/questions/2333400/what-can-be-the-reasons-of-connection-refused-errors
7. https://stackoverflow.com/questions/1449324/how-to-simulate-press-any-key-to-continue