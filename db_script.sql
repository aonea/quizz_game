create table if not exists qa(
    id            int primary key not null,
    question      text not null,
    option_1      text not null,
    option_2      text not null,
    option_3      text not null,
    answer        int  check (answer >= 1 and answer <=3)            
);

insert into qa
values (1, "Cati cai pot fi inhamati maxim la o trasura?", "6", "3", "2 dar frumosi", 1);

insert into qa
values (2, "In ce an s-a nascut Mihai Eminescu?", "1850", "1889", "1910", 1);

insert into qa
values (3, "In ce an a murit Mihai Eminescu?", "1850", "1889", "1915", 2);

insert into qa
values (4, "Cati ani dureaza studiile de licenta la FII minim?", "4", "5", "3", 3);

insert into qa
values (5, "Cat este salariul brut minim pe economie in Romania?", "750", "1250", "1050", 2);

insert into qa
values (6, "Care este ultima generatie de placi video nVidia?", "7", "12", "10", 3);

insert into qa
values (7, "In ce an a intrat Romania in U.E.?", "2005", "2003", "2004", 3);

insert into qa
values (8, "In cat timp va pica economia Romaniei cu actuala guvernare?", "acusi", "2 luni", "aprox. 5-6 ani", 1);

insert into qa
values (9, "Carui gen muzical apartine grupul $uicideBoy$?", "Emo", "Trap", "Jazz", 2);

insert into qa
values (10, "In ce cartier din L.A. a copilarit Kendrick Lamar?", "Compton", "South Central", "Piru", 1);

